# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import TYPE_CHECKING

from mautrix.types import (EventID, RoomID, UserID, Event, MessageEvent, StateEvent,
                           EncryptedEvent)
from mautrix.errors import MatrixError

from . import user as u
from .asprogram import BaseMatrixHandler
from .db import User as DBUser

if TYPE_CHECKING:
    from .__main__ import WBot


class MatrixHandler(BaseMatrixHandler):
    def __init__(self, asprogram: 'WBot') -> None:
        super().__init__(asprogram=asprogram)

    async def send_welcome_message(self, room_id: RoomID, inviter: 'u.User') -> None:
        await super().send_welcome_message(room_id, inviter)
        if not inviter.notice_room:
            inviter.notice_room = room_id
            await inviter.save()
            await self.send_notice(room_id, "This room has been marked as your "
                                            "W-Bot control room.")

    async def handle_invite(self, room_id: RoomID, user_id: UserID, invited_by: 'u.User',
                            event_id: EventID) -> None:
        # TODO Forbid inviting non-Supporters? Or only kick on join?
        pass

    async def handle_join(self, room_id: RoomID, user_id: UserID, _: EventID) -> None:
        user = await u.User.get_by_mxid(user_id)

        # TODO Block users?
        #if not user.is_whitelisted:
        #    await self.az.intent.kick_user(room_id, user.mxid,
        #                                   "You are not whitelisted on this "
        #                                   "appservice.")
        #    return
        if not await user.is_logged_in():
            # TODO Better message, and auto-invite user to a DM where they can log in!
            await self.az.intent.kick_user(room_id, user.mxid,
                                           "You are not logged in to Newgrounds Passport as a Supporter.")
            return

        self.log.debug(f"{user.mxid} joined {room_id}")

    async def handle_leave(self, room_id: RoomID, user_id: UserID, event_id: EventID) -> None:
        try:
            if len(await self.az.intent.get_room_members(room_id)) == 1:
                self.log.info(f"Appservice bot leaving empty room")
                await self.az.intent.leave_room(room_id)
                await DBUser.delete_by_notice_room(room_id)
        except MatrixError:
            self.log.exception(f"Failed to get member list of {room_id}")

    async def handle_reject(self, room_id: RoomID, user_id: UserID, reason: str, event_id: EventID) -> None:
        await self.handle_leave(room_id, user_id, event_id)

    def filter_matrix_event(self, evt: Event) -> bool:
        if not isinstance(evt, (MessageEvent, StateEvent, EncryptedEvent)):
            return True
        return super().filter_matrix_event(evt)
