# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional

from . import session
from .base import (NGSubmissionInfo,
                   NGShareParser, NGShareErrorParser,
                   NGEmbedParser, NGEmbedException,
                   NGPortalParser,
                   NGURL_SHARE, NGURL_EMBED, NGURL_PVIEW)


async def get_submission_info(submission_id: int) -> NGSubmissionInfo:
    info = NGSubmissionInfo(id=submission_id)

    async with session.get(NGURL_SHARE.format(submission_id)) as r:
        if r.status != 200:
            error_parser = NGShareErrorParser()
            error_parser.feed(await r.text())
            raise NGEmbedException(error_parser.error_message or "Unknown error")

        NGShareParser(info).feed(await r.text())

    if info.embed_code:
        async with session.get(NGURL_EMBED.format(info.embed_code)) as r:
            if r.status != 200:
                raise NGEmbedException("Couldn't reach embed link")

            NGEmbedParser(info).feed(await r.text())

    if not info.title:
        async with session.get(NGURL_PVIEW.format(submission_id)) as r:
            if r.status != 200:
                raise NGEmbedException("Couldn't reach submission page")

            NGPortalParser(info).feed(await r.text())

    return info

async def load_img(img_url: str) -> Optional[bytes]:
    if img_url:
        async with session.get(img_url) as r:
            return await r.read() if r.status == 200 else None
    else:
        return None