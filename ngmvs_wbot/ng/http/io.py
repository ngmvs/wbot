# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional, Dict
import json

from . import session


NGIO_URL = "https://www.newgrounds.io/gateway_v3.php/"
NGIO_HEADERS = {"Content-Type": "application/x-www-form-urlencoded"}
APP_ID = "" # Must be set at runtime!


class NGIOException(Exception):
    def __init__(self, message: str, code: Optional[int] = None):
        super().__init__(message)
        self.code = code

    def __str__(self, *args, **kwargs):
        suffix = f" ({self.code})" if self.code is not None else ""
        return super().__str__() + suffix


# TODO Allow posting multiple components at once (up to 10)
async def _call_component(component: str, session_id: Optional[str] = None,
                          parameters: Optional[Dict] = None
                          ) -> Dict:
    call_obj = {
        "component": component
    }
    if parameters:
        call_obj["parameters"] = parameters

    input_obj = {
        "app_id": APP_ID,
        "call": call_obj,
    }
    if session_id:
        input_obj["session_id"] = session_id

    async with session.post(
        NGIO_URL,
        headers=NGIO_HEADERS,
        data={"input": json.dumps(input_obj)}
    ) as r:
        r.raise_for_status()

        error_obj = None
        output_obj = await r.json()
        if not output_obj["success"]:
            error_obj = output_obj["error"]
        else:
            result = output_obj["result"]
            if not result:
                raise NGIOException("No result(s) returned")
            elif "data" in result and not result["data"]["success"]:
                error_obj = result["data"]["error"]

        if error_obj:
            raise NGIOException(
                error_obj.get('message','Unknown error'),
                error_obj.get('code'))

        return result["data"]


async def is_logged_in(session_id: Optional[str]) -> bool:
    if session_id is None:
        return False
    session = await check_session(session_id)
    return not session["expired"] and session["user"]

async def check_session(session_id: str) -> Dict:
    data = await _call_component("App.checkSession", session_id)
    return data["session"]

async def start_session() -> Dict:
    data = await _call_component("App.startSession")
    return data["session"]

async def end_session(session_id: str) -> None:
    await _call_component("App.endSession", session_id)