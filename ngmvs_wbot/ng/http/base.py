# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional
from attr import dataclass
from html.parser import HTMLParser


NGURL_SHARE = "https://www.newgrounds.com/content/share/{:d}/1"
NGURL_EMBED = "https://www.newgrounds.com/content/embed.php?id=S{:s}"
NGURL_PVIEW = "https://www.newgrounds.com/portal/view/{:d}"


class NGEmbedException(Exception):
    def __init__(self, message):
        super().__init__(message)


@dataclass
class NGSubmissionInfo:
    id: int
    title: str = ""
    desc: str = ""
    img_url: str = ""
    embed_code: str = ""
    # Assume iframe sizes and prefix letters are stable:
    # small (S):  400x225
    # medium (M): 640x360
    # large (L):  800x450
    media_type: str = "movie"
    # TODO Use these somewhere
    author: str = ""
    author_url: str = ""

    @property
    def url(self):
        return NGURL_PVIEW.format(self.id)

class NGShareParser(HTMLParser):
    info: NGSubmissionInfo

    def __init__(self, info: Optional[NGSubmissionInfo] = None):
        super().__init__()
        self.info = info if info else NGSubmissionInfo()

        self._finished = False

    def handle_starttag(self, tag, attrs):
        if self._finished:
            return
        elif tag == "input" and ("type", "text") in attrs:
            for name, value in attrs:
                if name == "value" and value.startswith("<iframe"):
                    embed_code_start = value.find("?id=")
                    if embed_code_start != -1:
                        embed_code_start += 5
                        embed_code_end = value.find('"', embed_code_start)
                        self.info.embed_code = value[embed_code_start:embed_code_end]
                        self._finished = True
                        break

class NGShareErrorParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.error_message = None

        self._found_error_div = False
        self._found_error_p = False
        self._finished = False

    def handle_starttag(self, tag, attrs):
        if self._finished:
            return
        elif not self._found_error_div:
            if tag == "div" and ("class", "fatal-error") in attrs:
                self._found_error_div = True
        else:
            if tag == "p":
                self._found_error_p = True
                
    def handle_data(self, data):
        if self._finished:
            return
        elif self._found_error_p:
            self.error_message = data
            self._finished = True


class NGEmbedParser(HTMLParser):
    info: NGSubmissionInfo

    def __init__(self, info: Optional[NGSubmissionInfo] = None):
        super().__init__()
        self.info = info if info else NGSubmissionInfo()

        self._in_details = False
        self._in_title = False
        self._in_author = False
        self._in_desc = False
        self._finished = False

    def handle_starttag(self, tag, attrs):
        if self._finished:
            return
        elif not self._in_details:
            if tag == "div":
                if ("class", "icon") in attrs:
                    img_url = attrs[1][1]
                    img_url = img_url[img_url.find("https://"):img_url.find(")")]
                    self.info.img_url = img_url
                elif ("class", "details") in attrs:
                    self._in_details = True
                elif ("class", "error") in attrs:
                    self._finished = True
        else:
            if tag == "h2" and not self.info.title:
                self._in_title = True
            if tag == "a" and not self.info.author:
                self._in_author = True
                self.info.author_url = attrs[0][1].replace("http://", "https://", 1)
            elif tag == "strong":
                self._in_desc = True

    def handle_data(self, data):
        if self._finished:
            return
        elif self._in_title:
            self.info.title = data
        elif self._in_author:
            self.info.author = data
        elif self._in_desc:
            self.info.desc = data

    def handle_endtag(self, tag):
        if self._finished:
            return
        elif self._in_details:
            if tag != "div":
                self._in_title = False
                self._in_author = False
                self._in_desc = False
            else:
                self._finished = True


class NGPortalParser(HTMLParser):
    info: NGSubmissionInfo

    def __init__(self, info: Optional[NGSubmissionInfo] = None):
        super().__init__()
        self.info = info if info else NGSubmissionInfo()

        self._finished = False

    def handle_starttag(self, tag, attrs):
        if self._finished:
            return
        elif tag == "meta":
            if ("name", "title") in attrs:
                self.info.title = attrs[1][1]
            elif ("name", "description") in attrs:
                self.info.desc = attrs[1][1]
            elif ("property", "og:type") in attrs:
                self.info.media_type = attrs[1][1]
            elif ("property", "og:image") in attrs:
                img_url = attrs[1][1]
                query_idx = img_url.find("?")
                if query_idx != -1:
                    img_url = img_url[:query_idx]
                img_url = img_url.replace("_card", "")
                self.info.img_url = img_url

    def handle_endtag(self, tag):
        if self._finished:
            return
        elif tag == "head":
            self._finished = True