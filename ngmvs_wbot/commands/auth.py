# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import asyncio

from mautrix.types.event.message import TextMessageEventContent, MessageType,\
    RelationType, RelatesTo

from ..asprogram.commands import HelpSection, command_handler
from ..ng.http import io as ngio
from .typehint import CommandEvent
from aiohttp.client_exceptions import ServerDisconnectedError

SECTION_AUTH = HelpSection("Authentication", 10, "")


@command_handler(needs_auth=False, management_only=False, help_section=SECTION_AUTH,
                 help_text="Log in via Newgrounds Passport")
async def login(evt: CommandEvent) -> None:
    if await evt.sender.is_logged_in():
        await evt.reply("You're already logged in!")
    else:
        session = await ngio.start_session()
        reply_id = await evt.reply(f"[Click here]({session['passport_url']}) to log in via Newgrounds Passport!")

        checker_task = asyncio.create_task(_check_session_loop(evt))
        evt.sender.command_status = {
            "action": "Login",
            "session_id": session["id"],
            "reply_id": reply_id,
            "checker_task": checker_task,
        }

async def _check_session_loop(evt: CommandEvent) -> None:
    orig_cmd_status = evt.sender.command_status
    if not orig_cmd_status or orig_cmd_status["action"] != "Login":
        return
    session_id = orig_cmd_status["session_id"]

    while True:
        await asyncio.sleep(3)
        if evt.sender.command_status is not orig_cmd_status:
            evt.az.intent.redact(evt.room_id, session_id["reply_id"])
            return

        final_message = None
        suggest_retry = True
        try:
            session = await ngio.check_session(session_id)
        except ServerDisconnectedError as e:
            evt.log.exception("Server disconnected, try again")
            continue
        except ngio.NGIOException as e:
            if e.code == 111:
                final_message = "You cancelled the login session."
                suggest_retry = False
            else:
                final_message = f"Something went wrong: {e} "
        except Exception as e:
            final_message = "An unknown error has occurred. "
            evt.log.exception(f"Unknown error occurred while checking for session to log in: {e}")
        else:
            if not session or session["expired"]:
                final_message = "The login link has expired. "

        if evt.sender.command_status is not orig_cmd_status:
            evt.log.info("Login session was cancelled in the middle of checking it")
            evt.az.intent.redact(evt.room_id, session_id["reply_id"])
            if session["user"]:
                asyncio.create_task(ngio.end_session(session_id))
            return

        if not final_message:
            user = session["user"]
            if user:
                suggest_retry = False
                if not user["supporter"]:
                    final_message = (
                        "It looks like you aren't a Newgrounds Supporter! "
                        "The Newgrounds MVS is open only to Newgrounds Supporters. "
                        "[Become a Supporter today!](https://www.newgrounds.com/supporter)")
                    asyncio.create_task(ngio.end_session(session_id))
                else:
                    final_message = (
                        f"Hi, **{user['name']}**! You logged in successfully.\n"
                        "You may now join rooms managed by the Newgrounds MVS.")
                    await evt.sender.on_logged_in(user["id"], session_id, session["remember"])
            else:
                # Still waiting for a valid user session: keep looping
                continue

        if final_message:
            evt.sender.command_status = None
            if suggest_retry:
                final_message += "Try running the `$cmdprefix+sp login` command again."
            await evt.reply(
                final_message,
                relates_to=RelatesTo(
                    rel_type=RelationType.REPLACE,
                    event_id=orig_cmd_status["reply_id"])
                )
            return


@command_handler(needs_auth=True, help_section=SECTION_AUTH,
                 help_text="Log out of Newgrounds Passport")
async def logout(evt: CommandEvent) -> None:
    if await evt.sender.logout():
        await evt.reply("Successfully logged out.")
    else:
        await evt.reply("Failed to log out...try again later.")