# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional, Dict, Any
from aiohttp.client_exceptions import ClientConnectorError, ClientConnectionError

import magic

from mautrix.appservice.state_store.asyncpg import PgASStateStore
from mautrix.errors.request import MUnknown
from mautrix.types import JSON, RoomAlias, Event, EventType, StateEvent, Membership, UserID
from mautrix.types.misc import RoomDirectoryVisibility
from mautrix.types.primitive import ContentURI
from mautrix.util.async_db import Database
from mautrix.util.simple_template import SimpleTemplate

from .ng.http import client as ng_client, io as ngio
from .ng.http.base import NGSubmissionInfo, NGEmbedException

from .asprogram import ASProgram
from .config import Config
from .db import upgrade_table, init as init_db
from .user import User
from .matrix import MatrixHandler
from .version import version, linkified_version
from . import commands as _


class WBot(ASProgram):
    name = "ngmvs-wbot"
    module = "ngmvs_wbot"
    command = "python -m ngmvs_wbot"
    description = "Newgrounds integration on the Matrix network."
    repo_url = "https://gitlab.com/ngmvs/wbot"
    real_user_content_key = "com.gitlab.ngmvs.wbot"
    version = version
    markdown_version = linkified_version
    config_class = Config
    matrix_class = MatrixHandler

    db: Database
    config: Config
    matrix: MatrixHandler
    state_store: PgASStateStore

    mxid_template: SimpleTemplate[int]

    def make_state_store(self) -> None:
        self.state_store = PgASStateStore(self.db)

    def prepare_db(self) -> None:
        self.db = Database(self.config["appservice.database"], upgrade_table=upgrade_table,
                           loop=self.loop, db_args=self.config["appservice.database_opts"])
        init_db(self.db)

    def prepare(self) -> None:
        super().prepare()
        ngio.APP_ID = self.config["ng.app_id"]
        self.mxid_template = SimpleTemplate(self.config["asprogram.alias_template"], "roomid",
                                            prefix="#", suffix=f":{self.az.domain}", type=int)
        self.az.query_alias = self._create_portal_room
        self.add_startup_actions(User.init_cls(self))

    def prepare_stop(self) -> None:
        super().prepare_stop()
        # TODO
        #self.add_shutdown_actions(User.stop_cls())

    # TODO Is there even any state to save on exit?
    #async def stop(self) -> None:
    #    await super().stop()
    #    self.log.debug("Saving user sessions")
    #    for user in User.by_mxid.values():
    #        await user.save()

    async def start(self) -> None:
        await self.db.start()
        await self.state_store.upgrade_table.upgrade(self.db.pool)
        if self.matrix.e2ee:
            self.matrix.e2ee.crypto_db.override_pool(self.db.pool)
        # TODO Should portal rooms be tracked in DB, via a Portal class?
        await super().start()

    async def get_user(self, user_id: UserID, create: bool = True) -> User:
        return await User.get_by_mxid(user_id, create=create)

    async def count_logged_in_users(self) -> int:
        return len([user for user in User.by_ngid.values() if user.ngid])

    async def manhole_global_namespace(self, user_id: UserID) -> Dict[str, Any]:
        return {
            **await super().manhole_global_namespace(user_id),
            "User": User,
        }


    async def _create_portal_room(self, room_alias: RoomAlias) -> JSON:
        submission_id: Optional[int] = self.mxid_template.parse(room_alias)
        try:
            if submission_id is None:
                raise ValueError(f"Unable to parse integer submission ID from room alias of {room_alias}")
            info = await ng_client.get_submission_info(submission_id)
            # TODO Lock?
            await self._create_matrix_room(info)
            return True
        except (NGEmbedException, ValueError) as e:
            # TODO Consider sending DM to user for an unsupported submission
            self.log.error(f"Invalid room alias: {e}")
        except (ClientConnectionError, ClientConnectorError) as e:
            self.log.error(f"Connection error: {e}")
        return None

    async def _create_matrix_room(self, info: NGSubmissionInfo) -> None:
        # TODO Check if room exists already? Or rely on aliases?
        self.log.debug("Creating Matrix room")

        initial_state = [
            {
                "type": str(EventType.ROOM_HISTORY_VISIBILITY),
                "content": {
                    "history_visibility": "world_readable"
                }
            },
            {
                "type": "m.room.guest_access",
                "content": {
                    "guest_access": "forbidden"
                }
            },
            {
                "type": str(EventType.ROOM_JOIN_RULES),
                "content": {
                    "join_rule": "restricted",
                    "allow": [{
                        "type": "m.room_membership",
                        "room_id": self.config["ng.required_membership"]
                    }]
                } if self.config["ng.required_membership"] else {
                    "join_rule": "public",
                }
            }
        ]

        icon_mxc = await self._upload_thumbnail(info)
        if icon_mxc:
            initial_state.append({
                "type": str(EventType.ROOM_AVATAR),
                "content": {
                    "url": icon_mxc
                }
            })

        widget_type = f"ng.wbot.{info.media_type}"
        widget_id = f"{widget_type}.{info.embed_code or info.id}"
        if info.embed_code:
            widget_url = f"{self.config['ng.portal_viewer_url']}/viewport.php?id={info.embed_code}"
        else:
            widget_url = f"{self.config['ng.portal_viewer_url']}/linkonly.php?id={info.id}&type={info.media_type}&title={info.title}"
        initial_state.extend([
            {
                "type": "im.vector.modular.widgets",
                "state_key": widget_id,
                "content": {
                    "type": widget_type,
                    "url": widget_url,
                    "name": info.title,
                    "creatorUserId": self.az.bot_mxid,
                    "id": widget_id,
                    "avatar_url": self.config["ng.portal_viewer_favicon"]
                }
            },
            {
                "type": "io.element.widgets.layout",
                "content": {
                    "widgets": {
                        widget_id: {
                            "container": "top",
                            "height": 75,
                            "width": 100,
                            "index": 0,
                        }
                    }
                }
            }
        ])

        mxid = await self.az.intent.create_room(
            alias_localpart=self.mxid_template.format(info.id),
            initial_state=initial_state,
            name=info.title, topic=f"{info.desc} | {info.url}")

        if not mxid:
            raise Exception("Failed to create room: no mxid returned")

        self.log.debug(f"Matrix room created: {mxid}")

        try:
            await self.az.intent.set_room_directory_visibility(
                mxid, RoomDirectoryVisibility.PUBLIC)
        except MUnknown as e:
            self.log.warning(f"Unable to publish {mxid} to room directory: {e}")

        try:
            if self.config[f"ng.portal_space.{info.media_type}"]:
                await self.az.intent.send_state_event(
                    self.config[f"ng.portal_space.{info.media_type}"],
                    EventType.SPACE_CHILD,
                    {
                        "via": [ self.az.domain ],
                        "suggested": False,
                        "auto_join": False
                    },
                    mxid)
        except Exception as e:
            self.log.error(f"Failed to add room {mxid} to {info.media_type} space: {e}")

    async def _upload_thumbnail(self, info) -> ContentURI:
        data = await ng_client.load_img(info.img_url)
        if data:
            mime = magic.from_buffer(data, mime=True)
            return await self.az.intent.upload_media(data, mime_type=mime)
        else:
            return ContentURI("")


WBot().run()