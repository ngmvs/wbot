# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import (Dict, List, Optional, AsyncIterable, Awaitable, Union, TypeVar, AsyncGenerator,
                    TYPE_CHECKING, cast)
import asyncio

from mautrix.types import (UserID, RoomID, EventID,
                           TextMessageEventContent, MessageType)

from .ng.http import io as ngio

from .asprogram import BaseUser, async_getter_lock
from .config import Config
from .db import User as DBUser

if TYPE_CHECKING:
    from .__main__ import WBot

try:
    from aiohttp_socks import ProxyError, ProxyConnectionError, ProxyTimeoutError
except ImportError:
    class ProxyError(Exception):
        pass


    ProxyConnectionError = ProxyTimeoutError = ProxyError

class User(DBUser, BaseUser):
    config: Config

    by_mxid: Dict[UserID, 'User'] = {}
    by_ngid: Dict[int, 'User'] = {}

    _notice_room_lock: asyncio.Lock
    _notice_send_lock: asyncio.Lock
    is_admin: bool
    permission_level: str
    # no difference between "logged in" and "connected"...?

    def __init__(self, mxid: UserID, ngid: Optional[int] = None,
                 session_id: Optional[int] = None, keepalive: bool = False,
                 notice_room: Optional[RoomID] = None) -> None:
        super().__init__(
            mxid=mxid, ngid=ngid,
            session_id=session_id, keepalive=keepalive,
            notice_room=notice_room)
        BaseUser.__init__(self)
        self.notice_room = notice_room
        self._notice_room_lock = asyncio.Lock()
        self._notice_send_lock = asyncio.Lock()
        self.command_status = None
        (self.is_whitelisted, self.is_admin,
         self.permission_level) = self.config.get_permissions(mxid)

        self.log = self.log.getChild(self.mxid)

    @classmethod
    def init_cls(cls, asprogram: 'WBot') -> AsyncIterable[Awaitable[bool]]:
        cls.asprogram = asprogram
        cls.config = asprogram.config
        cls.az = asprogram.az
        cls.loop = asprogram.loop
        # no community_helper or disconnect_notices
        return (user.load_session() async for user in cls.all_logged_in())

    # no is_connected

    # region Database getters
    
    def _add_to_cache(self) -> None:
        self.by_mxid[self.mxid] = self
        if self.ngid:
            self.by_ngid[self.ngid] = self

    @classmethod
    async def all_logged_in(cls) -> AsyncGenerator['User', None]:
        users = await super().all_logged_in()
        user: cls
        for user in users:
            try:
                yield cls.by_mxid[user.mxid]
            except KeyError:
                user._add_to_cache()
                yield user

    @classmethod
    @async_getter_lock
    async def get_by_mxid(cls, mxid: UserID, *, create: bool = True) -> Optional['User']:
        if mxid == cls.az.bot_mxid:
            return None
        try:
            return cls.by_mxid[mxid]
        except KeyError:
            pass

        user = cast(cls, await super().get_by_mxid(mxid))
        if user is not None:
            user._add_to_cache()
            return user

        if create:
            cls.log.debug(f"Creating user instance for {mxid}")
            user = cls(mxid)
            await user.insert()
            user._add_to_cache()
            return user

        return None

    @classmethod
    @async_getter_lock
    async def get_by_ngid(cls, ngid: int) -> Optional['User']:
        try:
            return cls.by_ngid[ngid]
        except KeyError:
            pass

        user = cast(cls, await super().get_by_ngid(ngid))
        if user is not None:
            user._add_to_cache()
            return user

        return None

    # endregion

    # TODO Kick out logged-out users
    async def load_session(self) -> bool:
        attempt = 0
        while True:
            try:
                is_logged_in = await self.is_logged_in()
                break
            except (ProxyError, ProxyTimeoutError, ProxyConnectionError, ConnectionError) as e:
                attempt += 1
                wait = min(attempt * 10, 60)
                self.log.warning(f"{e.__class__.__name__} while trying to restore session, "
                                 f"retrying in {wait} seconds: {e}")
                await asyncio.sleep(wait)
            except Exception as e:
                self.log.exception("Failed to restore session: {e}")
                return False
        if is_logged_in:
            self.log.info("Loaded session successfully")
            #self.stop_keepalive()
            asyncio.create_task(self.post_login())
            return True
        return False

    async def is_logged_in(self) -> bool:
        try:
            return await ngio.is_logged_in(self.session_id)
        except Exception:
            self.log.exception("Exception checking login status")
            return False

    # no refresh, _reload_session, reconnect

    async def logout(self) -> bool:
        if self.session_id:
            try:
                ngio.end_session(self.session_id)
            except Exception as e:
                self.log.exception(f"Failed to log out: {e}")
                return False
            self.session_id = None

        if self.ngid:
            try:
                del self.by_ngid[self.ngid]
            except KeyError:
                pass
            self.ngid = None

        #self.stop_keepalive()
        self.keepalive = False

        await self.save()
        return True

    async def post_login(self) -> None:
        self.log.info("Running post-login actions")
        self._add_to_cache()

        #self.start_keepalive()

    # no _create_community, _add_community, _add_community_puppet
    # no get_direct_chats: just use room membership
    # no sync_threads
    # no _mute_room
    # no is_in_portal
    # no on_2fa_callback

    async def get_notice_room(self) -> RoomID:
        if not self.notice_room:
            async with self._notice_room_lock:
                # If someone already created the room while this call was waiting,
                # don't make a new room
                if self.notice_room:
                    return self.notice_room
                self.notice_room = await self.az.intent.create_room(
                    is_direct=True, invitees=[self.mxid],
                    topic="W-Bot control room")
                await self.save()
        return self.notice_room

    async def send_bot_notice(self, text: str, edit: Optional[EventID] = None,
                                 important: bool = False, error_code: Optional[str] = None,
                                 error_message: Optional[str] = None) -> Optional[EventID]:
        # no disable_bridge_notices
        event_id = None
        try:
            self.log.debug("Sending bot notice: %s", text)
            content = TextMessageEventContent(body=text, msgtype=(MessageType.TEXT if important
                                                                  else MessageType.NOTICE))
            if edit:
                content.set_edit(edit)
            # This is locked to prevent notices going out in the wrong order
            async with self._notice_send_lock:
                event_id = await self.az.intent.send_message(await self.get_notice_room(), content)
        except Exception:
            self.log.warning("Failed to send bot notice", exc_info=True)
        return edit or event_id

    # TODO If needed, implement bridge state methods

    # no start_listen / start_keepalive
    # no _disconnect_keepalive_task_after_error / _disconnect_keepalive_task_after_error
    # no _update_seq_id, _update_region_hint
    # no _try_listen / _try_keepalive
    # no on_connect
    # no on_disconnect
    # no stop_listen / stop_keepalive

    async def on_logged_in(self, ngid: str, session_id: str, keepalive: bool) -> None:
        self.log.debug(
            f"Successfully logged in as {ngid} "
            f"(session_id: {session_id}, keepalive: {'yes' if keepalive else 'no'})")
        self.ngid = ngid
        self.session_id = session_id
        self.keepalive = keepalive
        await self.save()
        #self.stop_listen()
        asyncio.create_task(self.post_login())
