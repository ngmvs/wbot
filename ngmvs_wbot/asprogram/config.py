# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Dict, Optional, List, Any
from abc import ABC
import secrets
import time
import re

from mautrix.util.config import (BaseFileConfig, ConfigUpdateHelper, BaseValidatableConfig,
                                 ForbiddenDefault, yaml)


class BaseASProgramConfig(BaseFileConfig, BaseValidatableConfig, ABC):
    """Like mautrix.bridge.BaseBridgeConfig, but without bridging."""

    registration_path: str
    _registration: Optional[Dict]
    _check_tokens: bool

    def __init__(self, path: str, registration_path: str, base_path: str) -> None:
        super().__init__(path, base_path)
        self.registration_path = registration_path
        self._registration = None
        self._check_tokens = True

    def save(self) -> None:
        super().save()
        if self._registration and self.registration_path:
            with open(self.registration_path, "w") as stream:
                yaml.dump(self._registration, stream)

    @staticmethod
    def _new_token() -> str:
        return secrets.token_urlsafe(48)

    @property
    def forbidden_defaults(self) -> List[ForbiddenDefault]:
        return [
            ForbiddenDefault("homeserver.address", "https://example.com"),
            ForbiddenDefault("homeserver.domain", "example.com"),
        ] + ([
            ForbiddenDefault("appservice.as_token",
                             "This value is generated when generating the registration",
                             "Did you forget to generate the registration?"),
            ForbiddenDefault("appservice.hs_token",
                             "This value is generated when generating the registration",
                             "Did you forget to generate the registration?"),
        ] if self._check_tokens else [])

    def do_update(self, helper: ConfigUpdateHelper) -> None:
        copy = helper.copy

        copy("homeserver.address")
        copy("homeserver.domain")
        copy("homeserver.verify_ssl")
        copy("homeserver.http_retry_count")
        #copy("homeserver.status_endpoint")

        copy("appservice.address")
        copy("appservice.hostname")
        copy("appservice.port")
        copy("appservice.max_body_size")

        copy("appservice.tls_cert")
        copy("appservice.tls_key")

        copy("appservice.database")
        copy("appservice.database_opts")

        copy("appservice.id")
        copy("appservice.bot_username")
        copy("appservice.bot_displayname")
        copy("appservice.bot_avatar")

        copy("appservice.as_token")
        copy("appservice.hs_token")

        copy("appservice.ephemeral_events")

        copy("manhole.enabled")
        copy("manhole.path")
        copy("manhole.whitelist")

        copy("logging")

    @property
    def namespaces(self) -> Dict[str, List[Dict[str, Any]]]:
        """
        Generate the user ID and room alias namespace config for the registration as specified in
        https://matrix.org/docs/spec/application_service/r0.1.0.html#application-services
        """
        homeserver = self["homeserver.domain"]
        regex_ph = f"regexplaceholder{int(time.time())}"
        username_format = (self["asprogram.username_template"].format(userid=regex_ph)
                           if "asprogram.username_template" in self else None)
        alias_format = (self["asprogram.alias_template"].format(roomid=regex_ph)
                        if "asprogram.alias_template" in self else None)
        # no group_id

        return {
            "users": [{
                "exclusive": True,
                "regex": re.escape(f"@{username_format}:{homeserver}").replace(regex_ph, ".*"),
            }] if username_format else [],
            "aliases": [{
                "exclusive": True,
                "regex": re.escape(f"#{alias_format}:{homeserver}").replace(regex_ph, ".*"),
            }] if alias_format else []
        }

    def generate_registration(self) -> None:
        self["appservice.as_token"] = self._new_token()
        self["appservice.hs_token"] = self._new_token()

        namespaces = self.namespaces
        bot_username = self["appservice.bot_username"]
        homeserver_domain = self["homeserver.domain"]
        namespaces.setdefault("users", []).append({
            "exclusive": True,
            "regex": re.escape(f"@{bot_username}:{homeserver_domain}"),
        })

        self._registration = {
            "id": self["appservice.id"],
            "as_token": self["appservice.as_token"],
            "hs_token": self["appservice.hs_token"],
            "namespaces": namespaces,
            "url": self["appservice.address"],
            "sender_localpart": self._new_token(),
            "rate_limited": False
        }

        if self["appservice.ephemeral_events"]:
            self._registration["de.sorunome.msc2409.push_ephemeral"] = True
            self._registration["push_ephemeral"] = True
