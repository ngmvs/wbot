# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional, Type, Dict, Any
from abc import ABC, abstractmethod
import sys

from mautrix.types import UserID
from mautrix.appservice import AppService, ASStateStore
from mautrix.api import HTTPAPI
from mautrix.util.program import Program
from mautrix import __version__ as __mautrix_version__

from .config import BaseASProgramConfig
from .commands.manhole import ManholeState
from .matrix import BaseMatrixHandler

try:
    from mautrix.appservice.state_store.sqlalchemy import SQLASStateStore
    from mautrix.util.db import Base

    import sqlalchemy as sql
    from sqlalchemy.engine.base import Engine
except ImportError:
    Base = SQLASStateStore = sql = Engine = None


class ASProgram(Program, ABC):
    """
    Like mautrix.bridge.Bridge, but without bridging.
    Users are Matrix users who may have DMs with and issue commands to the bridge.
    """
    
    db: 'Engine'
    az: AppService
    state_store_class: Type[ASStateStore] = SQLASStateStore
    state_store: ASStateStore
    config_class: Type[BaseASProgramConfig]
    config: BaseASProgramConfig
    matrix_class: Type[BaseMatrixHandler]
    matrix: BaseMatrixHandler
    repo_url: str
    markdown_version: str
    real_user_content_key: Optional[str] = None
    manhole: Optional[ManholeState]

    def __init__(self, module: str = None, name: str = None, description: str = None,
                 command: str = None, version: str = None,
                 real_user_content_key: Optional[str] = None,
                 config_class: Type[BaseASProgramConfig] = None,
                 matrix_class: Type[BaseMatrixHandler] = None,
                 state_store_class: Type[ASStateStore] = None) -> None:
        super().__init__(module, name, description, command, version, config_class)
        if real_user_content_key:
            self.real_user_content_key = real_user_content_key
        if matrix_class:
            self.matrix_class = matrix_class
        if state_store_class:
            self.state_store_class = state_store_class
        self.manhole = None

    def prepare_arg_parser(self) -> None:
        super().prepare_arg_parser()
        self.parser.add_argument("-g", "--generate-registration", action="store_true",
                                 help="generate registration and quit")
        self.parser.add_argument("-r", "--registration", type=str, default="registration.yaml",
                                 metavar="<path>",
                                 help="the path to save the generated registration to (not needed "
                                      "for running the appservice)")

    def preinit(self) -> None:
        super().preinit()
        if self.args.generate_registration:
            self.generate_registration()
            sys.exit(0)

    def prepare(self) -> None:
        super().prepare()
        self.prepare_db()
        self.prepare_appservice()
        self.prepare_asprogram()

    def prepare_config(self) -> None:
        self.config = self.config_class(self.args.config, self.args.registration,
                                        self.args.base_config)
        if self.args.generate_registration:
            self.config._check_tokens = False
        self.load_and_update_config()

    def generate_registration(self) -> None:
        self.config.generate_registration()
        self.config.save()
        print(f"Registration generated and saved to {self.config.registration_path}")

    def make_state_store(self) -> None:
        if self.state_store_class is None:
            raise RuntimeError("state_store_class is not set")
        else:
            self.state_store = self.state_store_class()

    def prepare_appservice(self) -> None:
        self.make_state_store()
        mb = 1024 ** 2
        default_http_retry_count = self.config.get("homeserver.http_retry_count", None)
        self.az = AppService(server=self.config["homeserver.address"],
                             domain=self.config["homeserver.domain"],
                             verify_ssl=self.config["homeserver.verify_ssl"],

                             id=self.config["appservice.id"],
                             as_token=self.config["appservice.as_token"],
                             hs_token=self.config["appservice.hs_token"],

                             tls_cert=self.config.get("appservice.tls_cert", None),
                             tls_key=self.config.get("appservice.tls_key", None),

                             bot_localpart=self.config["appservice.bot_username"],
                             ephemeral_events=self.config["appservice.ephemeral_events"],

                             default_ua=f"{self.name}/{self.version} {HTTPAPI.default_ua}",
                             default_http_retry_count=default_http_retry_count,

                             log="mau.as",
                             loop=self.loop,

                             state_store=self.state_store,

                             real_user_content_key=self.real_user_content_key,

                             aiohttp_params={
                                 "client_max_size": self.config["appservice.max_body_size"] * mb
                             })
        # TODO If needed, implement this & all of the state methods on .asprogram.BaseUser
        #self.az.app.router.add_post("/_matrix/app/com.beeper.asprogram_state", self.get_asprogram_state)

    def prepare_db(self) -> None:
        if not sql:
            raise RuntimeError("SQLAlchemy is not installed")
        self.db = sql.create_engine(self.config["appservice.database"],
                                    **self.config["appservice.database_opts"])
        Base.metadata.bind = self.db
        if not self.db.has_table("alembic_version"):
            self.log.critical("alembic_version table not found. "
                              "Did you forget to `alembic upgrade head`?")
            sys.exit(10)

    def prepare_asprogram(self) -> None:
        self.matrix = self.matrix_class(asprogram=self)

    async def start(self) -> None:
        self.log.debug("Starting appservice...")
        await self.az.start(self.config["appservice.hostname"], self.config["appservice.port"])
        await self.matrix.wait_for_connection()

        # no status_endpoint

        await self.matrix.init_encryption()
        self.add_startup_actions(self.matrix.init_as_bot())
        await super().start()
        self.az.ready = True

    async def stop(self) -> None:
        if self.manhole:
            self.manhole.close()
            self.manhole = None
        await self.az.stop()
        await super().stop()
        if self.matrix.e2ee:
            await self.matrix.e2ee.stop()

    # no get_asprogram_state

    @abstractmethod
    async def get_user(self, user_id: UserID, create: bool = True) -> 'BaseUser':
        pass

    @abstractmethod
    async def count_logged_in_users(self) -> int:
        return 0

    async def manhole_global_namespace(self, user_id: UserID) -> Dict[str, Any]:
        own_user = await self.get_user(user_id, create=False)
        return {
            "asprogram": self,
            "manhole": self.manhole,
            "own_user": own_user,
        }

    @property
    def manhole_banner_python_version(self) -> str:
        return f"Python {sys.version} on {sys.platform}"

    @property
    def manhole_banner_program_version(self) -> str:
        return f"{self.name} {self.version} with mautrix-python {__mautrix_version__}"

    def manhole_banner(self, user_id: UserID) -> str:
        return (f"{self.manhole_banner_python_version}\n"
                f"{self.manhole_banner_program_version}\n\n"
                f"Manhole opened by {user_id}\n")