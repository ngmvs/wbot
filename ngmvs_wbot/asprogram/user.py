# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Dict, Any, Optional, List, TYPE_CHECKING
from collections import defaultdict
from abc import ABC, abstractmethod
import logging
import asyncio

from mautrix.appservice import AppService
from mautrix.types import UserID
from mautrix.util.logging import TraceLogger
from mautrix.util.opt_prometheus import Gauge

if TYPE_CHECKING:
    from .asprogram import ASProgram


class BaseUser(ABC):
    """
    Like mautrix.bridge.BaseUser, but without bridging.
    """

    log: TraceLogger = logging.getLogger("mau.user")
    _async_get_locks: Dict[Any, asyncio.Lock] = defaultdict(lambda: asyncio.Lock())
    az: AppService
    asprogram: 'ASProgram'
    loop: asyncio.AbstractEventLoop

    is_whitelisted: bool
    is_admin: bool
    mxid: UserID

    dm_update_lock: asyncio.Lock
    command_status: Optional[Dict[str, Any]]
    _metric_value: Dict[Gauge, bool]

    def __init__(self) -> None:
        self.dm_update_lock = asyncio.Lock()
        self.command_status = None
        self._metric_value = defaultdict(lambda: False)
        self.log = self.log.getChild(self.mxid)

    @abstractmethod
    async def is_logged_in(self) -> bool:
        raise NotImplementedError()

    # no direct_chats: not so important to explicitly mark 1:1 rooms as direct; this isn't a bridge

    def _track_metric(self, metric: Gauge, value: bool) -> None:
        if self._metric_value[metric] != value:
            if value:
                metric.inc(1)
            else:
                metric.dec(1)
            self._metric_value[metric] = value

    # TODO get/fill/push_asprogram_state, if needed