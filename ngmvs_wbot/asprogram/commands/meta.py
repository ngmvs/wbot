# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Dict, List

from mautrix.types import EventID

from .handler import (HelpSection, HelpCacheKey, command_handler, CommandEvent, command_handlers,
                      SECTION_GENERAL)


@command_handler(needs_auth=False, help_section=SECTION_GENERAL,
                 help_text="Cancel an ongoing action.")
async def cancel(evt: CommandEvent) -> EventID:
    if evt.sender.command_status:
        action = evt.sender.command_status["action"]
        evt.sender.command_status = None
        return await evt.reply(f"{action} cancelled.")
    else:
        return await evt.reply("No ongoing command.")


@command_handler(needs_auth=False, help_section=SECTION_GENERAL,
                 help_text="Get the appservice version.")
async def version(evt: CommandEvent) -> None:
    if not evt.processor.asprogram:
        await evt.reply("Appservice version unknown")
    else:
        await evt.reply(f"[{evt.processor.asprogram.name}]({evt.processor.asprogram.repo_url}) "
                        f"{evt.processor.asprogram.markdown_version or evt.processor.asprogram.version}")


@command_handler(needs_auth=False)
async def unknown_command(evt: CommandEvent) -> EventID:
    return await evt.reply("Unknown command. Try `$cmdprefix+sp help` for help.")


help_cache: Dict[HelpCacheKey, str] = {}


async def _get_help_text(evt: CommandEvent) -> str:
    cache_key = await evt.get_help_key()
    if cache_key not in help_cache:
        help_sections: Dict[HelpSection, List[str]] = {}
        for handler in command_handlers.values():
            if handler.has_help and handler.has_permission(cache_key):
                help_sections.setdefault(handler.help_section, [])
                help_sections[handler.help_section].append(handler.help + "  ")
        help_sorted = sorted(help_sections.items(), key=lambda item: item[0].order)
        helps = ["#### {}\n{}\n".format(key.name, "\n".join(value)) for key, value in help_sorted]
        help_cache[cache_key] = "\n".join(helps)
    return help_cache[cache_key]


def _get_management_status(evt: CommandEvent) -> str:
    if evt.is_management:
        return "This is a management room: prefixing commands with `$cmdprefix` is not required."
    else:
        return "**This is not a management room**: you must prefix commands with `$cmdprefix`."


@command_handler(name="help", needs_auth=False, help_section=SECTION_GENERAL,
                 help_text="Show this help message.")
async def help_cmd(evt: CommandEvent) -> EventID:
    return await evt.reply(_get_management_status(evt) + "\n" + await _get_help_text(evt))
