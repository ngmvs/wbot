# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from .handler import (command_handler, CommandEvent, SECTION_ADMIN)


@command_handler(needs_admin=True, needs_auth=False, help_section=SECTION_ADMIN,
                 help_text="Reset the bot's megolm session in this room")
async def discard_megolm_session(evt: CommandEvent) -> None:
    if not evt.bot.matrix.e2ee:
        await evt.reply("End-to-appservice encryption is not enabled on this appservice instance")
        return
    await evt.asprogram.matrix.e2ee.crypto_store.remove_outbound_group_session(evt.room_id)
    await evt.reply("Successfully removed outbound group session for this room")
