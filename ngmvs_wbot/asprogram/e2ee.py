# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Tuple, Union, Optional, Dict
import logging
import asyncio

from mautrix.types import (Filter, RoomFilter, EventFilter, RoomEventFilter, StateFilter, EventType,
                           RoomID, Serializable, JSON, MessageEvent, EncryptedEvent, StateEvent,
                           EncryptedMegolmEventContent, RequestedKeyInfo, RoomKeyWithheldCode,
                           LoginType)
from mautrix.appservice import AppService
from mautrix.errors import EncryptionError, SessionNotFound
from mautrix.client import Client, SyncStore
from mautrix.crypto import (OlmMachine, CryptoStore, StateStore, PgCryptoStore, PickleCryptoStore,
                            DeviceIdentity, RejectKeyShare, TrustState)
from mautrix.util.logging import TraceLogger

from .crypto_state_store import PgCryptoStateStore, SQLCryptoStateStore

try:
    from mautrix.util.async_db import Database
except ImportError:
    Database = None


class EncryptionManager:
    loop: asyncio.AbstractEventLoop
    log: TraceLogger = logging.getLogger("mau.asprogram.e2ee")

    client: Client
    crypto: OlmMachine
    crypto_store: Union[CryptoStore, SyncStore]
    crypto_db: Optional[Database]
    state_store: StateStore

    asprogram: 'ASProgram'
    az: AppService

    _share_session_events: Dict[RoomID, asyncio.Event]

    def __init__(self, asprogram: 'asprogram', homeserver_address: str,
                 db_url: str, key_sharing_config: Dict[str, bool] = None
                 ) -> None:
        self.loop = asprogram.loop or asyncio.get_event_loop()
        self.asprogram = asprogram
        self.az = asprogram.az
        self.device_name = asprogram.name
        self._share_session_events = {}
        self.key_sharing_config = key_sharing_config or {}
        pickle_key = "mautrix.asprogram.e2ee"
        if db_url.startswith("postgres://") or db_url.startswith("postgresql://"):
            if not PgCryptoStore or not PgCryptoStateStore:
                raise RuntimeError("Database URL is set to postgres, but asyncpg is not installed")
            self.crypto_db = Database(url=db_url, upgrade_table=PgCryptoStore.upgrade_table,
                                      log=logging.getLogger("mau.crypto.db"), loop=self.loop)
            self.crypto_store = PgCryptoStore("", pickle_key, self.crypto_db)
            self.state_store = PgCryptoStateStore(self.crypto_db)
        elif db_url.startswith("pickle:///"):
            self.crypto_db = None
            self.crypto_store = PickleCryptoStore("", pickle_key, db_url[len("pickle:///"):])
            self.state_store = SQLCryptoStateStore()
        else:
            raise RuntimeError("Unsupported database scheme")
        default_http_retry_count = asprogram.config.get("homeserver.http_retry_count", None)
        self.client = Client(base_url=homeserver_address, mxid=self.az.bot_mxid, loop=self.loop,
                             sync_store=self.crypto_store, log=self.log.getChild("client"),
                             default_retry_count=default_http_retry_count)
        self.crypto = OlmMachine(self.client, self.crypto_store, self.state_store)
        self.crypto.allow_key_share = self.allow_key_share

    async def allow_key_share(self, device: DeviceIdentity, request: RequestedKeyInfo) -> bool:
        require_verification = self.key_sharing_config.get("require_verification", True)
        allow = self.key_sharing_config.get("allow", False)
        if not allow:
            self.log.debug(f"Key sharing not enabled, ignoring key request from "
                           f"{device.user_id}/{device.device_id}")
            return False
        elif device.trust == TrustState.BLACKLISTED:
            raise RejectKeyShare(f"Rejecting key request from blacklisted device "
                                 f"{device.user_id}/{device.device_id}",
                                 code=RoomKeyWithheldCode.BLACKLISTED,
                                 reason="You have been blacklisted by this device")
        elif device.trust == TrustState.VERIFIED or not require_verification:
            # no portal
            self.log.debug(f"Accepting key request for {request.session_id} from "
                           f"{device.user_id}/{device.device_id}")
            return True
        else:
            raise RejectKeyShare(f"Rejecting key request from unverified device "
                                 f"{device.user_id}/{device.device_id}",
                                 code=RoomKeyWithheldCode.UNVERIFIED,
                                 reason="You have not been verified by this device")

    async def handle_member_event(self, evt: StateEvent) -> None:
        await self.crypto.handle_member_event(evt)

    async def _share_session_lock(self, room_id: RoomID) -> bool:
        try:
            event = self._share_session_events[room_id]
        except KeyError:
            self._share_session_events[room_id] = asyncio.Event()
            return True
        else:
            await event.wait()
            return False

    async def encrypt(self, room_id: RoomID, event_type: EventType,
                      content: Union[Serializable, JSON]
                      ) -> Tuple[EventType, EncryptedMegolmEventContent]:
        try:
            encrypted = await self.crypto.encrypt_megolm_event(room_id, event_type, content)
        except EncryptionError:
            self.log.debug("Got EncryptionError, sharing group session and trying again")
            if await self._share_session_lock(room_id):
                try:
                    # not get_members_filtered
                    users = await self.az.state_store.get_members(room_id)
                    await self.crypto.share_group_session(room_id, users)
                finally:
                    self._share_session_events.pop(room_id).set()
            encrypted = await self.crypto.encrypt_megolm_event(room_id, event_type, content)
        return EventType.ROOM_ENCRYPTED, encrypted

    async def decrypt(self, evt: EncryptedEvent, wait_session_timeout: int = 5) -> MessageEvent:
        try:
            decrypted = await self.crypto.decrypt_megolm_event(evt)
        except SessionNotFound as e:
            if not wait_session_timeout:
                raise
            self.log.debug(f"Couldn't find session {e.session_id} trying to decrypt {evt.event_id},"
                           f" waiting {wait_session_timeout} seconds...")
            got_keys = await self.crypto.wait_for_session(evt.room_id, e.sender_key, e.session_id,
                                                          timeout=wait_session_timeout)
            if got_keys:
                self.log.debug(f"Got session {e.session_id} after waiting, trying to decrypt {evt.event_id} again")
                decrypted = await self.crypto.decrypt_megolm_event(evt)
            else:
                raise
        self.log.trace("Decrypted event %s: %s", evt.event_id, decrypted)
        return decrypted

    async def check_server_support(self) -> bool:
        flows = await self.client.get_login_flows()
        return flows.supports_type(LoginType.APPSERVICE)

    async def start(self) -> None:
        self.log.debug("Logging in with appservice bot user")
        if self.crypto_db:
            await self.crypto_db.start()
        await self.crypto_store.open()
        device_id = await self.crypto_store.get_device_id()
        if device_id:
            self.log.debug(f"Found device ID in database: {device_id}")
        # We set the API token to the AS token here to authenticate the appservice login
        # It'll get overridden after the login
        self.client.api.token = self.az.as_token
        await self.client.login(login_type=LoginType.APPSERVICE, device_name=self.device_name,
                                device_id=device_id, store_access_token=True, update_hs_url=False)
        await self.crypto.load()
        if not device_id:
            await self.crypto_store.put_device_id(self.client.device_id)
            self.log.debug(f"Logged in with new device ID {self.client.device_id}")
        _ = self.client.start(self._filter)
        self.log.info("End-to-appservice encryption support is enabled")

    async def stop(self) -> None:
        self.client.stop()
        await self.crypto_store.close()
        if self.crypto_db:
            await self.crypto_db.stop()

    @property
    def _filter(self) -> Filter:
        all_events = EventType.find("*")
        return Filter(
            account_data=EventFilter(types=[all_events]),
            presence=EventFilter(not_types=[all_events]),
            room=RoomFilter(
                include_leave=False,
                state=StateFilter(not_types=[all_events]),
                timeline=RoomEventFilter(not_types=[all_events]),
                account_data=RoomEventFilter(not_types=[all_events]),
                ephemeral=RoomEventFilter(not_types=[all_events]),
            ),
        )