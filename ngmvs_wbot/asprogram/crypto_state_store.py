# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import List

from mautrix.types import RoomID, UserID
from mautrix.crypto import StateStore


try:
    from mautrix.client.state_store.sqlalchemy import SQLStateStore as SQLCryptoStateStore
except ImportError:
    SQLCryptoStateStore = None

try:
    from mautrix.util.async_db import Database
    from mautrix.client.state_store.asyncpg import PgStateStore


    class PgCryptoStateStore(PgStateStore, StateStore):
        def __init__(self, db: Database) -> None:
            super().__init__(db)

        async def find_shared_rooms(self, user_id: UserID) -> List[RoomID]:
            rows = await self.db.fetch("SELECT room_id FROM mx_user_profile "
                                       "LEFT JOIN portal ON portal.mxid=mx_user_profile.room_id "
                                       "WHERE user_id=$1 AND portal.encrypted=true", user_id)
            return [row["room_id"] for row in rows]
except ImportError:
    Database = None
    PgCryptoStateStore = None