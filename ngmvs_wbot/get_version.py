# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import subprocess
import shutil
import os

from . import __version__

cmd_env = {
    "PATH": os.environ["PATH"],
    "HOME": os.environ["HOME"],
    "LANG": "C",
    "LC_ALL": "C",
}


def run(cmd):
    return subprocess.check_output(cmd, stderr=subprocess.DEVNULL, env=cmd_env)


if os.path.exists(".git") and shutil.which("git"):
    try:
        git_revision = run(["git", "rev-parse", "HEAD"]).strip().decode("ascii")
        git_revision_url = f"https://github.com/mrjohnson22/ngmvs_wbot/commit/{git_revision}"
        git_revision = git_revision[:8]
    except (subprocess.SubprocessError, OSError):
        git_revision = "unknown"
        git_revision_url = None
else:
    git_revision = "unknown"
    git_revision_url = None

# This will never have any releases
git_tag_url = None
git_tag = None

if not __version__.endswith("+dev"):
    __version__ += "+dev"
version = f"{__version__}.{git_revision}"
if git_revision_url:
    linkified_version = f"{__version__}.[{git_revision}]({git_revision_url})"
else:
    linkified_version = version
