# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Any, Tuple, List
import os

from mautrix.types import UserID
from mautrix.util.config import ConfigUpdateHelper, ForbiddenDefault, ForbiddenKey

from .asprogram import BaseASProgramConfig


class Config(BaseASProgramConfig):
    def __getitem__(self, key: str) -> Any:
        try:
            return os.environ[f"NGMVS_WBOT_{key.replace('.', '_').upper()}"]
        except KeyError:
            return super().__getitem__(key)

    @property
    def forbidden_defaults(self) -> List[ForbiddenDefault]:
        return [
            *super().forbidden_defaults,
            ForbiddenDefault("appservice.database", "postgres://username:password@hostname/db"),
            ForbiddenDefault("asprogram.permissions", ForbiddenKey("example.com")),
        ]

    def do_update(self, helper: ConfigUpdateHelper) -> None:
        super().do_update(helper)
        copy, copy_dict, _ = helper

        #copy("homeserver.asmux")

        #copy("appservice.community_id")

        copy("metrics.enabled")
        copy("metrics.listen_port")

        copy("asprogram.alias_template")
        copy("asprogram.command_prefix")
        copy("asprogram.encryption.allow")
        copy("asprogram.encryption.default")
        copy("asprogram.encryption.key_sharing.allow")
        copy("asprogram.encryption.key_sharing.require_cross_signing")
        copy("asprogram.encryption.key_sharing.require_verification")

        copy("ng.app_id")
        copy("ng.required_membership")
        copy("ng.portal_space.movie")
        copy("ng.portal_space.game")
        copy("ng.portal_viewer_url")
        copy("ng.portal_viewer_favicon")

        copy_dict("asprogram.permissions")

    def _get_permissions(self, key: str) -> Tuple[bool, bool, str]:
        level = self["asprogram.permissions"].get(key, "")
        admin = level == "admin"
        user = level == "user" or admin
        return user, admin, level

    def get_permissions(self, mxid: UserID) -> Tuple[bool, bool, str]:
        permissions = self["asprogram.permissions"] or {}
        if mxid in permissions:
            return self._get_permissions(mxid)

        homeserver = mxid[mxid.index(":") + 1:]
        if homeserver in permissions:
            return self._get_permissions(homeserver)

        return self._get_permissions("*")