# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import Optional, List, ClassVar, TYPE_CHECKING

from attr import dataclass

from mautrix.types import UserID, RoomID
from mautrix.util.async_db import Database

fake_db = Database("") if TYPE_CHECKING else None


@dataclass
class User:
    db: ClassVar[Database] = fake_db

    mxid: UserID
    ngid: Optional[int]
    session_id: Optional[str]
    keepalive: bool
    notice_room: Optional[RoomID]

    @classmethod
    async def all_logged_in(cls) -> List['User']:
        rows = await cls.db.fetch('SELECT mxid, ngid, session_id, keepalive, notice_room FROM "user" '
                                  "WHERE ngid IS NOT NULL AND session_id IS NOT NULL")
        return [cls(**row) for row in rows]

    @classmethod
    async def get_by_ngid(cls, ngid: int) -> Optional['User']:
        q = 'SELECT mxid, ngid, session_id, keepalive, notice_room FROM "user" WHERE ngid=$1'
        row = await cls.db.fetchrow(q, ngid)
        if not row:
            return None
        return cls(**row)

    @classmethod
    async def get_by_mxid(cls, mxid: UserID) -> Optional['User']:
        q = 'SELECT mxid, ngid, session_id, keepalive, notice_room FROM "user" WHERE mxid=$1'
        row = await cls.db.fetchrow(q, mxid)
        if not row:
            return None
        return cls(**row)

    async def insert(self) -> None:
        q = ('INSERT INTO "user" (mxid, ngid, session_id, keepalive, notice_room) '
             'VALUES ($1, $2, $3, $4, $5)')
        await self.db.execute(q, self.mxid, self.ngid, self.session_id, self.keepalive, self.notice_room)

    async def delete(self) -> None:
        await self.db.execute('DELETE FROM "user" WHERE mxid=$1', self.mxid)

    @classmethod
    async def delete_by_notice_room(cls, notice_room: RoomID) -> None:
        await cls.db.execute('DELETE FROM "user" WHERE notice_room=$1', notice_room)

    async def save(self) -> None:
        q = ('UPDATE "user" '
             "SET ngid=$2, session_id=$3, keepalive=$4, notice_room=$5 "
             "WHERE mxid=$1")
        await self.db.execute(q, self.mxid, self.ngid, self.session_id, self.keepalive, self.notice_room)