# ngmvs-wbot: A Matrix appservice for integrating Newgrounds in Matrix rooms.
# Copyright (C) 2021 Xavier Johnson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from asyncpg import Connection

from mautrix.util.async_db import UpgradeTable

upgrade_table = UpgradeTable()


@upgrade_table.register(description="Initial revision")
async def upgrade_v1(conn: Connection) -> None:
    pass

@upgrade_table.register(description="Manage Newgrounds Passport sessions")
async def upgrade_passport(conn: Connection) -> None:
    await conn.execute("""CREATE TABLE "user" (
        mxid        TEXT PRIMARY KEY,
        ngid        INT,
        session_id  TEXT UNIQUE,
        keepalive   BOOLEAN NOT NULL DEFAULT false,
        notice_room TEXT
    )""")
