aiohttp>=3,<4
asyncpg>=0.20,<0.24
commonmark>=0.8,<0.10
mautrix>=0.10.2,<0.11
python-magic>=0.4,<0.5
ruamel.yaml>=0.15.94,<0.18
